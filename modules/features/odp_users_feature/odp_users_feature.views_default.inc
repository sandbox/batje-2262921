<?php
/**
 * @file
 * odp_users_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function odp_users_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'profile';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'profile';
  $view->human_name = 'profile';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'profile';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['columns'] = '3';
  $handler->display->display_options['row_plugin'] = 'entity';
  /* Field: Profile: Role */
  $handler->display->display_options['fields']['field_taxonomy']['id'] = 'field_taxonomy';
  $handler->display->display_options['fields']['field_taxonomy']['table'] = 'field_data_field_taxonomy';
  $handler->display->display_options['fields']['field_taxonomy']['field'] = 'field_taxonomy';
  $handler->display->display_options['fields']['field_taxonomy']['type'] = 'ds_taxonomy_view_mode';
  $handler->display->display_options['fields']['field_taxonomy']['settings'] = array(
    'target' => 'node',
    'depth' => '0',
    'hidden' => 1,
  );
  $handler->display->display_options['fields']['field_taxonomy']['delta_limit'] = 'all';
  /* Filter criterion: Profile: Role (field_taxonomy) */
  $handler->display->display_options['filters']['field_taxonomy_tid']['id'] = 'field_taxonomy_tid';
  $handler->display->display_options['filters']['field_taxonomy_tid']['table'] = 'field_data_field_taxonomy';
  $handler->display->display_options['filters']['field_taxonomy_tid']['field'] = 'field_taxonomy_tid';
  $handler->display->display_options['filters']['field_taxonomy_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_taxonomy_tid']['expose']['operator_id'] = 'field_taxonomy_tid_op';
  $handler->display->display_options['filters']['field_taxonomy_tid']['expose']['label'] = 'Role';
  $handler->display->display_options['filters']['field_taxonomy_tid']['expose']['operator'] = 'field_taxonomy_tid_op';
  $handler->display->display_options['filters']['field_taxonomy_tid']['expose']['identifier'] = 'field_taxonomy_tid';
  $handler->display->display_options['filters']['field_taxonomy_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_taxonomy_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_taxonomy_tid']['vocabulary'] = 'department';

  /* Display: Profile */
  $handler = $view->new_display('page', 'Profile', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['use_ajax'] = FALSE;
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['defaults']['query'] = FALSE;
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
    ),
    'field_taxonomy_tid' => array(
      'bef_format' => 'bef_links',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
  );
  $handler->display->display_options['exposed_form']['options']['input_required'] = 0;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['columns'] = '5';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'user_roles';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Profile: Role (field_taxonomy) */
  $handler->display->display_options['filters']['field_taxonomy_tid']['id'] = 'field_taxonomy_tid';
  $handler->display->display_options['filters']['field_taxonomy_tid']['table'] = 'field_data_field_taxonomy';
  $handler->display->display_options['filters']['field_taxonomy_tid']['field'] = 'field_taxonomy_tid';
  $handler->display->display_options['filters']['field_taxonomy_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_taxonomy_tid']['expose']['operator_id'] = 'field_taxonomy_tid_op';
  $handler->display->display_options['filters']['field_taxonomy_tid']['expose']['label'] = 'Filter on';
  $handler->display->display_options['filters']['field_taxonomy_tid']['expose']['operator'] = 'field_taxonomy_tid_op';
  $handler->display->display_options['filters']['field_taxonomy_tid']['expose']['identifier'] = 'field_taxonomy_tid';
  $handler->display->display_options['filters']['field_taxonomy_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_taxonomy_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_taxonomy_tid']['vocabulary'] = 'department';
  $handler->display->display_options['path'] = 'profile';

  /* Display: users_from_organisation */
  $handler = $view->new_display('block', 'users_from_organisation', 'block_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Profile: Full Names */
  $handler->display->display_options['fields']['field_full_name']['id'] = 'field_full_name';
  $handler->display->display_options['fields']['field_full_name']['table'] = 'field_data_field_full_name';
  $handler->display->display_options['fields']['field_full_name']['field'] = 'field_full_name';
  $handler->display->display_options['fields']['field_full_name']['label'] = '';
  $handler->display->display_options['fields']['field_full_name']['element_label_colon'] = FALSE;
  /* Field: Profile: Role */
  $handler->display->display_options['fields']['field_taxonomy']['id'] = 'field_taxonomy';
  $handler->display->display_options['fields']['field_taxonomy']['table'] = 'field_data_field_taxonomy';
  $handler->display->display_options['fields']['field_taxonomy']['field'] = 'field_taxonomy';
  $handler->display->display_options['fields']['field_taxonomy']['label'] = '';
  $handler->display->display_options['fields']['field_taxonomy']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_taxonomy']['type'] = 'ds_taxonomy_view_mode';
  $handler->display->display_options['fields']['field_taxonomy']['settings'] = array(
    'target' => 'node',
    'depth' => '0',
    'hidden' => 1,
  );
  $handler->display->display_options['fields']['field_taxonomy']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_taxonomy']['delta_offset'] = '0';
  /* Field: Profile: Profile Picture */
  $handler->display->display_options['fields']['field_profile_picture']['id'] = 'field_profile_picture';
  $handler->display->display_options['fields']['field_profile_picture']['table'] = 'field_data_field_profile_picture';
  $handler->display->display_options['fields']['field_profile_picture']['field'] = 'field_profile_picture';
  $handler->display->display_options['fields']['field_profile_picture']['label'] = '';
  $handler->display->display_options['fields']['field_profile_picture']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_profile_picture']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_profile_picture']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Profile: Organisation (field_organisation) */
  $handler->display->display_options['arguments']['field_organisation_tid']['id'] = 'field_organisation_tid';
  $handler->display->display_options['arguments']['field_organisation_tid']['table'] = 'field_data_field_organisation';
  $handler->display->display_options['arguments']['field_organisation_tid']['field'] = 'field_organisation_tid';
  $handler->display->display_options['arguments']['field_organisation_tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_organisation_tid']['default_argument_type'] = 'taxonomy_tid';
  $handler->display->display_options['arguments']['field_organisation_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_organisation_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_organisation_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  $export['profile'] = $view;

  return $export;
}
