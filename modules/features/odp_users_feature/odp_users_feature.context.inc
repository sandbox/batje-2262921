<?php
/**
 * @file
 * odp_users_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function odp_users_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'userprofile';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'all',
      ),
    ),
    'views' => array(
      'values' => array(
        'profile:page' => 'profile:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-profile-block_1' => array(
          'module' => 'views',
          'delta' => 'profile-block_1',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['userprofile'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'users_content';
  $context->description = '';
  $context->tag = 'pages';
  $context->conditions = array(
    'taxonomy_term' => array(
      'values' => array(
        'organisation' => 'organisation',
      ),
      'options' => array(
        'term_form' => '1',
      ),
    ),
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'all',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-users_content-block' => array(
          'module' => 'views',
          'delta' => 'users_content-block',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('pages');
  $export['users_content'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'wwwdu_organisation_pages';
  $context->description = '';
  $context->tag = 'wwwdu';
  $context->conditions = array(
    'taxonomy_term' => array(
      'values' => array(
        'organisation' => 'organisation',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-profile-block_2' => array(
          'module' => 'views',
          'delta' => 'profile-block_2',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('wwwdu');
  $export['wwwdu_organisation_pages'] = $context;

  return $export;
}
