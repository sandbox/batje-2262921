api = 2
core = 7.x

; Include the definition for how to build Drupal core directly, including patches:
; This is the 'should' way, but our jenkins server does drush make http:// and that
; does not work with includes. So we patch from within this file
;includes[] = drupal-org-core.make

projects[drupal][version] = 7.28

; Pull the project from git
projects[opendataportal][type] = "profile"
projects[opendataportal][download][type] = "git"
projects[opendataportal][download][branch] = "7.x-1.x"
projects[opendataportal][download][url]  = "http://git.drupal.org/sandbox/batje/2262921.git"
