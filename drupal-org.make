; Drupal.org release file.
core = 7.x
api = 2

; Basic contributed modules.

projects[ctools][version] = 1.4
projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"

projects[admin_menu][version] = 3.0-rc4
projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"

projects[ds][version] = 2.6
projects[ds][type] = "module"
projects[ds][subdir] = "contrib"

projects[context][version] = 3.2
projects[context][type] = "module"
projects[context][subdir] = "contrib"

projects[features][version] = 2.0
projects[features][type] = "module"
projects[features][subdir] = "contrib"

projects[features_extra][version] = 1.0-beta1
projects[features_extra][type] = "module"
projects[features_extra][subdir] = "contrib"

projects[icon][version] = 1.0-beta3
projects[icon][type] = "module"
projects[icon][subdir] = "contrib"

projects[oauth][version] = 3.2
projects[oauth][type] = "module"
projects[oauth][subdir] = "contrib"

projects[blockify][version] = 1.2
projects[blockify][type] = "module"
projects[blockify][subdir] = "contrib"

projects[bootstrap_tour][version] = 1.0-beta6
projects[bootstrap_tour][type] = "module"
projects[bootstrap_tour][subdir] = "contrib"

projects[entity][version] = 1.3
projects[entity][type] = "module"
projects[entity][subdir] = "contrib"

projects[google_groups][type] = "module"
projects[google_groups][subdir] = "contrib"
projects[google_groups][download][type] = "git"
projects[google_groups][download][branch] = 7.x-3.x
projects[google_groups][download][url] = "http://git.drupal.org/project/google_groups.git"

projects[less][version] = 3.0
projects[less][type] = "module"
projects[less][subdir] = "contrib"

projects[multiblock][version] = 1.1
projects[multiblock][type] = "module"
projects[multiblock][subdir] = "contrib"

projects[pane][version] = 2.3
projects[pane][type] = "module"
projects[pane][subdir] = "contrib"

projects[entityreference][version] = 1.1
projects[entityreference][type] = module
projects[entityreference][subdir] = "contrib"

projects[strongarm][version] = 2.0
projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"

projects[twitter][version] = 5.8
projects[twitter][type] = "module"
projects[twitter][subdir] = "contrib"

projects[twitter_block][version] = 2.1
projects[twitter_block][type] = "module"
projects[twitter_block][subdir] = "contrib"

projects[panels][version] = 3.4
projects[panels][type] = "module"
projects[panels][subdir] = "contrib"

projects[social_media_links][version] = 1.2
projects[social_media_links][type] = "module"
progects[social_media_links][subdir] = "contrib"

projects[jquery_update][version] = 2.4
projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = "contrib"

projects[wysiwyg][version] = 2.2
projects[wysiwyg][type] = "module"
projects[wysiwyg][subdir] = "contrib"

projects[views][version] = 3.7
projects[views][type] = "module"
projects[views][subdir] = "contrib"

projects[webform][version] = 3.20
projects[webform][type] = "module"
projects[webform][subdir] = "contrib"

projects[chosen][version] = 2.0-beta4
projects[chosen][type] = "module"
projects[chosen][subdir] = "contrib"

projects[better_exposed_filters][version] = 3.0-beta4
projects[better_exposed_filters][type] = "module"
projects[better_exposed_filters][subdir] = "contrib"

projects[bootstrap_tour][version] = 1.0-beta8
projects[bootstrap_tour][type] = "module"
projects[bootstrap_tour][subdir] = "contrib"

projects[entity_view_mode][version] = 1.0-rc1
projects[entity_view_mode][type] = "module"
projects[entity_view_mode][subdir] = "contrib"

projects[exclude_node_title][version] = 1.7
projects[exclude_node_title][type] = "module"
projects[exclude_node_title][subdir] = "contrib"

projects[field_formatter_css_class][version] = 1.3
projects[field_formatter_css_class][type] = "module"
projects[field_formatter_css_class][subdir] = "contrib"

projects[fontawesome][version] = 1.0
projects[fontawesome][type] = "module"
projects[fontawesome][subdir] = "contrib"

projects[module_filter][version] = 2.0-alpha2
projects[module_filter][type] = "module"
projects[module_filter][subdir] = "contrib"

projects[panels_bootstrap_styles][version] = 1.0-alpha1
projects[panels_bootstrap_styles][type] = "module"
projects[panels_bootstrap_styles][subdir] = "contrib"

projects[profile2][version] = 1.3
projects[profile2][type] = "module"
projects[profile2][subdir] = "contrib"

projects[libraries][version] = 3.x-dev
projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"

; Base theme.
projects[bootstrap][version] = 3.0
projects[bootstrap][type] = "theme"

;Main theme
projects[bukasa][type] = "theme"
projects[bukasa][download][type] = "git"
projects[bukasa][download][branch] = 7.x-1.x
projects[bukasa][download][url] = "http://git.drupal.org/sandbox/batje/2262849.git"

; Libraries
libraries[less][download][type] = "file"
libraries[less][download][url] = "https://github.com/oyejorge/less.php/archive/master.zip"
libraries[less][directory_name] = "lessphp"
libraries[less][destination] = "libraries"

libraries[chosen][download][type] = "file"
libraries[chosen][download][url] = "https://github.com/harvesthq/chosen/archive/v1.1.0.zip"
libraries[chosen][directory_name] = "chosen"
libraries[chosen][destination] = "libraries"

libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.0/ckeditor_4.4.0_standard.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"

libraries[fontawesome][download][type] = "file"
libraries[fontawesome][download][url] = "https://github.com/FortAwesome/Font-Awesome/archive/master.zip"
libraries[fontawesome][directory_name] = "fontawesome"
libraries[fontawesome][destination] = "libraries"


